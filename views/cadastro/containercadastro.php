<div class="wrapper fadeInDown">
  <div id="formContent">
    <!-- Tabs Titles -->

    <!-- Icon -->
    <div class="fadeIn first">
      <h1> Cadastro </h1>
    </div>

    <!-- Login Form -->
    <form>
      <input type="text" id="login" class="fadeIn second" name="login" placeholder="Login">
      <input type="text" id="password" class="fadeIn second" name="password" placeholder="Senha">
      <input type="text" id="confirmpassword" class="fadeIn second" name="confirmpassword" placeholder="Confirmar Senha">
      <input type="text" id="name" class="fadeIn second" name="name" placeholder="Nome">
      <input type="text" id="razaosocial" class="fadeIn second" name="razaosocial" placeholder="Razão Social">
      <input type="text" id="cnpj" class="fadeIn third" name="cnpj" placeholder="CNPJ">
      <input type="text" id="Link" class="fadeIn third" name="Link" placeholder="Link">
      <input type="submit" class="fadeIn fourth" value="Acessar">
    </form>

  </div>
</div>