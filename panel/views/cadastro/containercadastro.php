<div class="wrapper fadeInDown">
  <div id="formContent">
    <img class="logo" src="images/sempreit.png" alt="">
    <!-- Tabs Titles -->

    <!-- Icon -->
    <div class="fadeIn first">
      <h2> Cadastro de Clientes </h2>
    </div>

    <!-- Login Form -->
    <form>
      <select  class="fadeIn second selectClientes" name="" id="">
        <option value="">Selecione um cliente</option>
        <option value="">CLIENTE X</option>
        <option value="">CLIENTE Y</option>
      </select>
      <input type="text" id="login" class="fadeIn second" name="login" placeholder="Login">
      <input type="password" id="password" class="fadeIn second" name="password" placeholder="Senha">
      <input type="password" id="confirmpassword" class="fadeIn second" name="confirmpassword" placeholder="Confirmar Senha">
      <input type="text" id="name" class="fadeIn second" name="name" placeholder="Nome">
      <input type="text" id="razaosocial" class="fadeIn second" name="razaosocial" placeholder="Razão Social">
      <input type="text" id="cnpj" class="fadeIn third" name="cnpj" placeholder="CNPJ">
      <input type="text" id="Link" class="fadeIn third" name="Link" placeholder="Link">
      <input type="submit" class="fadeIn fourth btnAlterar" value="Cadastrar">
    </form>

  </div>
</div>