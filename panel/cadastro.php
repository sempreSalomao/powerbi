<!DOCTYPE html>
<html lang="pt-br">
<head>
    <?php require_once("views/utils/head.php"); ?>
    <title>Sinapse | Cadastro</title>
</head>
<body>
    <?php require_once("views/cadastro/containercadastro.php"); ?>
    <?php require_once("views/cadastro/containerList.php"); ?>
    <?php require_once("views/utils/scripts.php"); ?>
</body>
</html>